    package com.example.terra_linda.convertisseur;

    import android.app.Activity;
    import android.os.StrictMode;
    import android.support.v7.app.ActionBarActivity;
    import android.os.Bundle;
    import android.util.Log;
    import android.view.Menu;
    import android.view.MenuItem;
    import android.view.View;
  //  import android.webkit.WebView;
    import android.widget.AdapterView;
    import android.widget.ArrayAdapter;
    import android.widget.Button;
    //import android.widget.ListView;
    import android.widget.Spinner;
    import android.widget.TextView;
    import android.widget.Toast;
    import org.json.JSONObject;



    public class MainActivity extends Activity {


        public Converter cal = new Converter();
        public String first_list_item;
        public String second_list_item;
        public String rate;
        public Double amount;
        public Double mrate;
        //private WebView mWebView;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            //mWebView = (WebView) findViewById(R.id.webView);

            //initialize
            Button ok = (Button) findViewById(R.id.click_ok);
            final TextView amount_from = (TextView) findViewById(R.id.amount_from);
            final TextView displayresult = (TextView) findViewById(R.id.display_amount);

            //first list items
            Spinner dynamicSpinner = (Spinner) findViewById(R.id.dynamic_spinner);
            final String[] currency = new String[]{"NGN", "GBP", "USD", "ZAR"};
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, currency);
            dynamicSpinner.setAdapter(adapter);

            dynamicSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {
                    Log.v("currency", (String) parent.getItemAtPosition(position));
                    first_list_item = (String) parent.getItemAtPosition(position);

                    Toast.makeText(getApplicationContext(), "Clicked on: " + first_list_item, Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    Toast.makeText(getApplicationContext(), "No click made ", Toast.LENGTH_SHORT).show();
                }
            });

            //second list items
            Spinner dynamicSpinner2 = (Spinner) findViewById(R.id.dynamic_spinner2);

            ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, currency);
            dynamicSpinner2.setAdapter(adapter2);

            dynamicSpinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {
                    Log.v("item", (String) parent.getItemAtPosition(position));

                    second_list_item = (String) parent.getItemAtPosition(position);

                    Toast.makeText(getApplicationContext(), "Clicked on: " + second_list_item, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    Toast.makeText(getApplicationContext(), "No click made  ", Toast.LENGTH_SHORT).show();
                }
            });


            //


        ok.setOnClickListener(new View.OnClickListener()

              {
                  @Override
                  public void onClick(View v) {
                      StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                      StrictMode.setThreadPolicy(policy);
                      String query = first_list_item + "_" + second_list_item;
                      try {
                          String url = "http://free.currencyconverterapi.com/api/v3/convert?q=" + query + "&compact=y";
                         // mWebView.loadUrl(url);
                          String result = cal.doHttpUrlConnectionAction(url);
                          JSONObject jsonObj = new JSONObject(result);
                          String value = jsonObj.getString(query);
                          JSONObject valObj = new JSONObject(value);
                           rate = valObj.getString("val");
                          Log.d("show me:", rate);


                          amount = Double.parseDouble(amount_from.getText().toString());
                          mrate=Double.parseDouble(rate);
                          Double final_result =cal.conversion_rate(amount, mrate);
                          String displayrate = final_result.toString();
                          displayresult.setText(displayrate);

                      } catch (Exception e) {
                          e.printStackTrace();
                          Log.e("oops",e.getMessage());
                      }

                  }
                              }

        );





    }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_main, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }

            return super.onOptionsItemSelected(item);
        }
    }





